from setuptools import setup

setup(
    name='some_package',
    version='1.0.1',
    description='My first package',
    author='Sanek',
    author_email='alexmephi13425@mail.ru',
    py_modules=['someprogram'],
)

