def search4letters(phrase:str, letters:str='aeiou'):
    return set(phrase).intersection(set(letters))
