import mysql.connector
from flask import Flask, request, render_template, escape
from someprogram import search4letters

from DBcm import UseDatabase

app = Flask(__name__)

app.config['db_config'] = {
    'host': '127.0.0.1',
    'user': 'headfirst',
    'password': 'headfirst',
    'database': 'headfirst',
}


@app.route('/')
def hello_world() -> str:
    return 'Hello, world!'


def log(request: 'flask_request', results: str) -> None:
    with UseDatabase(app.config['db_config']) as cursor:
        _SQL = """
        INSERT INTO log (phrase, letters, ip, browser_string, results)
        VALUES (%s, %s, %s, %s, %s);
        """
        cursor.execute(
            _SQL,
            (
                request.form['phrase'], request.form['letters'],
                request.remote_addr, str(request.user_agent), results,
            )
        )


@app.route('/search', methods=['POST'])
def search() -> 'html':
    letters = request.form['letters']
    phrase = request.form['phrase']
    results = str(search4letters(phrase=phrase, letters=letters))
    log(request, results)
    return render_template(
        'search4letters/results.html',
        letters=letters, results=results, phrase=phrase
    )


@app.route('/entry')
def entry() -> 'html':
    return render_template('search4letters/entry.html')


@app.route('/view-log')
def view_log() -> str:
    table_captions = ('Phrase', 'Letters', 'IP address', 'User agent', 'Results')

    with UseDatabase(app.config['db_config']) as cursor:
        _SQL = """
            SELECT  phrase, letters, ip, browser_string, results FROM log
        """
        cursor.execute(_SQL)

        log = cursor.fetchall()

    return render_template('search4letters/log.html',
                           table_captions=table_captions, log=log)


if __name__ == '__main__':
    app.run(debug=True)
